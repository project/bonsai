<div class="node<?php print ($sticky) ? " sticky" : ""; ?>"> 
  <?php if ($page == 0): ?> 
  <h2><a href="<?php print $node_url ?>" rel="bookmark" title="Permanent Link to <?php print $title ?>"><?php print $title ?></a></h2> 
  <?php endif; ?> 
  <small><?php print $submitted ?></small> 
  <div class="entry"> <?php print $content ?> </div> 
  <?php if ($links): ?> 
  <p class="postmetadata">Posted in <?php print $terms ?> | <?php print $links ?> &#187;</p> 
  <?php endif; ?> 
</div>
